# dvdisaster

data loss/scratch/aging protection for CD/DVD media. https://tracker.debian.org/dvdisaster

## Next step: burning!
* [cd burn site:debian.org](https://www.google.com/search?q=cd+burn+site:debian.org)
* [*BurnCd*](https://wiki.debian.org/BurnCd)
* [*CD / DVD / BD Burner Hardware compatibility*](https://wiki.debian.org/Burner)